% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   PLOT ROSBAG RESULTS
% 
%   Main Script used to plot estimation results from a real experiment.
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

% clear
% clc
% close all
format short

main_path = strcat(pwd,'/../');

addpath(genpath(strcat(main_path,'functions')));
addpath(genpath(strcat(main_path,'params')));
addpath(genpath(strcat(main_path,'matlab_rosbag-0.4.1-linux64')));

set(0, 'DefaultFigureRenderer', 'zbuffer');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                                       
                                                        % Define data paths
% Run one each time and the plot will overpose.         
% bagfile_name = 'paper_exp/heart/heart_2016-04-27-06-07-36.bag'; % 5 In
% bagfile_name = 'paper_exp/heart/heart_2016-04-27-06-10-05.bag'; % 5 In
% bagfile_name = 'paper_exp/heart/heart_2016-04-27-06-11-53.bag'; % 5 In
% bagfile_name = 'paper_exp/heart/heart_2016-04-27-06-13-32.bag'; % 5 In
% bagfile_name = 'paper_exp/heart/heart_2016-04-27-06-16-23.bag'; % 4
% bagfile_name = 'paper_exp/heart/heart_2016-04-27-06-18-02.bag'; % 5 In
% bagfile_name = 'paper_exp/heart/heart_2016-04-27-06-19-40.bag'; % 5 In
% bagfile_name = 'paper_exp/heart/heart_2016-04-27-06-23-47.bag'; % 5 In
% bagfile_name = 'paper_exp/heart/heart_2016-04-27-06-26-55.bag'; % 5 In
% bagfile_name = 'paper_exp/heart/heart_2016-04-27-06-31-12.bag'; % 4
% bagfile_name = 'paper_exp/heart/heart_2016-04-27-06-32-47.bag'; % 5 In
% bagfile_name = 'paper_exp/heart/heart_2016-04-27-06-34-38.bag'; % 5 In
% bagfile_name = 'paper_exp/heart/heart_2016-04-27-06-36-19.bag'; % 5 In
% bagfile_name = 'paper_exp/heart/heart_2016-04-27-06-39-11.bag'; % 5 In
% bagfile_name = 'paper_exp/heart/heart_2016-04-27-06-40-57.bag'; % 5 In
% bagfile_name = 'paper_exp/heart/heart_2016-04-27-06-43-39.bag'; % 5 In
% bagfile_name = 'paper_exp/heart/heart_2016-04-15-06-44-54.bag'; % 3
% bagfile_name = 'paper_exp/heart/heart_2016-04-15-06-46-57.bag'; % 4.5
% bagfile_name = 'paper_exp/heart/heart_2016-04-15-06-52-17.bag'; % 5 In 
% bagfile_name = 'paper_exp/heart/heart_2016-04-15-07-00-21.bag'; % fatal
% bagfile_name = 'paper_exp/heart/heart_2016-04-15-07-02-05.bag'; % 3
% bagfile_name = 'paper_exp/heart/heart_2016-04-15-07-05-30.bag'; % 5 In
% bagfile_name = 'paper_exp/heart/heart_2016-04-15-07-07-09.bag'; % 4
% bagfile_name = 'paper_exp/heart/heart_2016-04-15-07-10-48.bag'; % 5 In
% bagfile_name = 'paper_exp/heart/heart_2016-04-15-07-12-27.bag'; % 3
% bagfile_name = 'paper_exp/heart/heart_2016-04-15-07-14-10.bag'; % 4
% bagfile_name = 'paper_exp/heart/heart_2016-04-15-07-19-16.bag'; % 5 In
% bagfile_name = 'paper_exp/heart/heart_2016-04-15-07-20-47.bag'; % fatal 
% bagfile_name = 'paper_exp/heart/heart_2016-04-15-07-22-51.bag'; % 3
% bagfile_name = 'paper_exp/heart/heart_2016-04-15-07-27-43.bag'; % 5 In
% bagfile_name = 'paper_exp/heart/heart_2016-04-15-07-29-29.bag'; % 5
% bagfile_name = 'paper_exp/heart/heart_2016-04-15-07-31-29.bag'; % 5
% bagfile_name = 'paper_exp/heart/heart_2016-04-15-07-34-58.bag'; % 5 In
% bagfile_name = 'paper_exp/heart/heart_2016-04-15-07-38-01.bag'; % 4
% bagfile_name = 'paper_exp/heart/heart_2016-04-15-07-40-04.bag'; % 5 In
% bagfile_name = 'paper_exp/heart/heart_2016-04-15-07-42-07.bag'; % 5 
% % 
% % 
% bagfile_name = 'paper_exp/eight/eight_2016-04-27-06-46-27.bag'; % 5 In
% bagfile_name = 'paper_exp/eight/eight_2016-04-27-06-50-47.bag'; % 5 In
% bagfile_name = 'paper_exp/eight/eight_2016-04-27-06-52-11.bag'; % 4
% bagfile_name = 'paper_exp/eight/eight_2016-04-27-06-53-39.bag'; % 5 In
% bagfile_name = 'paper_exp/eight/eight_2016-04-27-06-55-07.bag'; % 5 In
% bagfile_name = 'paper_exp/eight/eight_2016-04-15-05-17-40.bag'; % fatal
% bagfile_name = 'paper_exp/eight/eight_2016-04-15-05-19-45.bag'; % 5 In
% bagfile_name = 'paper_exp/eight/eight_2016-04-15-05-21-09.bag'; % 5 In
% bagfile_name = 'paper_exp/eight/eight_2016-04-15-05-25-17.bag'; % 4
% bagfile_name = 'paper_exp/eight/eight_2016-04-15-05-58-09.bag'; % 4
% bagfile_name = 'paper_exp/eight/eight_2016-04-15-06-01-22.bag'; % fatal
% bagfile_name = 'paper_exp/eight/eight_2016-04-15-06-02-48.bag'; % 5 
% bagfile_name = 'paper_exp/eight/eight_2016-04-15-06-04-16.bag'; % 5 In
% bagfile_name = 'paper_exp/eight/eight_2016-04-15-06-05-46.bag'; % 5 In
% bagfile_name = 'paper_exp/eight/eight_2016-04-15-06-07-11.bag'; % 5 In
% bagfile_name = 'paper_exp/eight/eight_2016-04-15-06-09-36.bag'; % 5 In
% bagfile_name = 'paper_exp/eight/eight_2016-04-15-06-10-49.bag'; % 3 
% bagfile_name = 'paper_exp/eight/eight_2016-04-15-06-12-04.bag'; % 5 In
% bagfile_name = 'paper_exp/eight/eight_2016-04-15-06-13-29.bag'; % fatal
% bagfile_name = 'paper_exp/eight/eight_2016-04-15-06-15-05.bag'; % 5 In
% bagfile_name = 'paper_exp/eight/eight_2016-04-15-06-33-54.bag'; % 5 In
% bagfile_name = 'paper_exp/eight/eight_2016-04-15-06-35-18.bag'; % 5 In
% bagfile_name = 'paper_exp/eight/eight_2016-04-15-06-36-29.bag'; % 5 In
% bagfile_name = 'paper_exp/eight/eight_2016-04-15-06-38-04.bag'; % 4
% bagfile_name = 'paper_exp/eight/eight_2016-04-15-06-39-17.bag'; % 3 not considered
% bagfile_name = 'paper_exp/eight/eight_2016-04-15-06-40-31.bag'; % 5 In

bagfile_name = 'paper_exp/fast/loop/loop_20s_2016-04-27-04-07-42.bag'; % Loop plot in the paper (10min flight)
% bagfile_name = 'paper_exp/fast/loop/loop_20s_2016-04-24-10-42-41.bag';
% bagfile_name = 'paper_exp/fast/loop/loop_20s_2016-04-16-07-18-55.bag';
% bagfile_name = 'paper_exp/plot_pos_ori.bag'; % Pos ori paper plots. extract 1/2 initial z offset
                                                 folder = '~/experiments/';
                                                    ns = '/QuadrotorOscar';
                         plotparams.topics.gtodom = '/QuadrotorOscar/odom';
                  plotparams.topics.estodom = strcat(ns,'/eskf_odom/odom');
                 plotparams.topics.imu = strcat(ns,'/quad_decode_msg/imu');
                plotparams.topics.px4flow = strcat(ns,'/px4flow/opt_flow');
           plotparams.topics.range = strcat(ns,'/teraranger/timeofflight');                             
%__________________________________________________________________________
                                                 % Plot position GT and EST
                                              plotparams.pos.enable = false;
%__________________________________________________________________________
                                              % Plot orientation GT and EST
                                              plotparams.ori.enable = false;                                           
%__________________________________________________________________________
                                                 % Plot velocity GT and EST
                                              plotparams.vel.enable = false;                                           
%__________________________________________________________________________

                                                     % Plot sensor readings
                                         plotparams.sensors.enable = false;
%__________________________________________________________________________

                                                     % Plot 3D trajectories
                                             plotparams.traj.enable = false;
                                           plotparams.traj.arena = 'empty';
                                           plotparams.traj.draw_every = 10;
                        plotparams.traj.scene_size = [-2 3.1 -2.5 2.5 0 5];
                                             plotparams.traj.view.az = -50;
                                              plotparams.traj.view.el = 20;
                              plotparams.traj.scene_center = [0.0;0.0;0.0];
                                         plotparams.rob.frame_length = 0.2;
                                             plotparams.rob.size_m2m = 0.2;
                                             
%__________________________________________________________________________

                                                     % Plot 2D trajectories
                                             plotparams.traj2D.enable = true;
                                                                                    
                                             
%___________________________________________________6000:end_______________________
                                            
                                     % Plot position and Orientation errors
                                         plotparams.trajerr.enable = false;
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Generate data path
if strfind('~',folder(1))
    path = strcat(folder,bagfile_name);
else
    path = strcat(main_path,folder,bagfile_name);
end
    
% Get all robots from specified path
robots = rosbag_wrapper_getrobots(path,plotparams);

% Synchronize robots (same time of samples)
for ii=1:size(robots,2)  
    robots(ii) = robotsync(robots(ii),robots(1));
end;


% Plot Odom estimation position
if plotparams.pos.enable
    fig = 1;
    x1 = robots(1).t;
    x2 = robots(2).t;
    y1 = robots(1).state(1:3,:);
    y2 = robots(2).state(1:3,:) + repmat(robots(1).state(1:3,1),1,size(robots(2).state,2));
    y2(3,:) = y2(3,:)-repmat(y2(3,1),1,length(y2)); % remove Z offset due to take off
    %y2(3,:) = y2(3,:)-0.5*repmat(y2(3,1),1,length(y2)); % Pos - Ori paper plots 
    hfig = figure(fig);
    hold on
    
    fontsize = 30;
    linewidth = 2;
    markersize = 10;
    markertype = '*';
    markerevery = 200;
    figsize = [100 100 1280 480];
    
    h(1) = plot(x1,y1(1,:),'r','LineWidth',linewidth); 
    h(2) = plotwmarker(x2,y2(1,:),linewidth,markersize,markertype,markerevery,'r');    
    h(3) = plot(x1,y1(2,:),'g','LineWidth',linewidth);
    h(4) = plotwmarker(x2,y2(2,:),linewidth,markersize,markertype,markerevery,'g');
    h(5) = plot(x1,y1(3,:),'b','LineWidth',linewidth);
    h(6) = plotwmarker(x2,y2(3,:),linewidth,markersize,markertype,markerevery,'b');
     
    miny1 = min(min(y1(1,:)),min(min(y1(2,:)),min(y1(3,:))));
    miny2 = min(min(y2(1,:)),min(min(y2(2,:)),min(y2(3,:))));
    miny = min(miny1,miny2);
    maxy1 = max(max(y1(1,:)),max(max(y1(2,:)),max(y1(3,:))));
    maxy2 = max(max(y2(1,:)),max(max(y2(2,:)),max(y2(3,:))));
    maxy = max(maxy1,maxy2);
    ylim([miny maxy]);
    
    xlim([x1(1) x1(end)]);
    
    xlabel('s');  
    ylabel('m');
    set_figure_params(hfig,'Estimation Position',fontsize,figsize);
    hleg = legend(h,'$x_t$','$\hat{x}$','$y_t$','$\hat{y}$','$z_t$','$\hat{z}$');
    set(hleg,'Interpreter','latex','Fontsize',fontsize,'Location','eastoutside');
    drawnow
    l = legendmarkeradjust(hleg,linewidth,markersize);
    hold off
end

%% Plot Odom estimation orientation
if plotparams.ori.enable
    fig = 2;
    x1 = robots(1).t;
    x2 = robots(2).t;
    for ii=1:length(x1)
        y1(1:3,ii) = q2e(robots(1).state(7:10,ii)) - q2e(robots(1).state(7:10,100));
    end
    for ii=1:length(x2)
        y2(1:3,ii) = q2e(robots(2).state(7:10,ii));
    end
   
    hfig = figure(fig);
    hold on
    
    avoid_last = 1000; 
    fontsize = 30;
    linewidth = 2;
    markersize = 10;
    markertype = '*';
    markerevery = 100;
    figsize = [100 100 1280 480];    

% %  Big loop stuff  
%     y1(2,4500:end) = y1(2,4500:end) - repmat(mean(y1(2,4500:end)),1,length(y1(2,4500:end)));
%     y1 = y1(:,1:end-avoid_last);
%     y2 = y2(:,1:end-avoid_last);
%     x1 = x1(:,1:end-avoid_last);
%     x2 = x2(:,1:end-avoid_last);
% 
%     y1(1,:) = medfilt1(y1(1,:),100);
%     y1(2,:) = medfilt1(y1(2,:),100);
%     y2(1,:) = medfilt1(y2(1,:),100);
%     y2(2,:) = medfilt1(y2(2,:),100);    
%     
%     y2(1,:) = 0.3*(y2(1,:)-mean(y2(1,:)))+mean(y2(1,:));
%     y2(2,:) = 0.2*(y2(2,:)-mean(y2(2,:)))+mean(y2(2,:));
     
    h(1) = plot(x1,y1(1,:),'r','LineWidth',linewidth); 
    h(2) = plotwmarker(x2,y2(1,:),linewidth,markersize,markertype,markerevery,'r');    
    h(3) = plot(x1,y1(2,:),'g','LineWidth',linewidth);
    h(4) = plotwmarker(x2,y2(2,:),linewidth,markersize,markertype,markerevery,'g');
    h(5) = plot(x1,y1(3,:),'b','LineWidth',linewidth);
    h(6) = plotwmarker(x2,y2(3,:),linewidth,markersize,markertype,markerevery,'b');   

    miny1 = min(min(y1(1,:)),min(min(y1(2,:)),min(y1(3,:))));
    miny2 = min(min(y2(1,:)),min(min(y2(2,:)),min(y2(3,:))));
    miny = min(miny1,miny2);
    maxy1 = max(max(y1(1,:)),max(max(y1(2,:)),max(y1(3,:))));
    maxy2 = max(max(y2(1,:)),max(max(y2(2,:)),max(y2(3,:))));
    maxy = max(maxy1,maxy2);
    ylim([miny maxy]);
    
    xlim([x1(1) x1(end)]);    
    
    xlabel('s');  
    ylabel('rad');    
    set_figure_params(hfig,'Estimation Orientation',fontsize,figsize);
    hleg = legend(h,'$\phi_{t}$','$\hat{\phi}$','$\theta_{t}$','$\hat{\theta}$','$\psi_{t}$','$\hat{\psi}$');
    set(hleg,'Interpreter','latex','Fontsize',fontsize,'Location','eastoutside');
    drawnow
    l = legendmarkeradjust(hleg,linewidth,markersize);
    hold off
end


%% Plot Odom estimation velocities
if plotparams.vel.enable
    fig = 3;
    x1 = robots(1).t;
    x2 = robots(2).t;
    y1 = robots(1).state(4:6,:);
    y2 = robots(2).state(4:6,:);

    hfig = figure(fig);
    hold on
    
    fontsize = 30;
    linewidth = 2;
    markersize = 10;
    markertype = '*';
    markerevery = 200;
    figsize = [100 100 1280 480];
    
    h(1) = plot(x1,y1(1,:),'r','LineWidth',linewidth); 
    h(2) = plotwmarker(x2,y2(1,:),linewidth,markersize,markertype,markerevery,'r');    
    h(3) = plot(x1,y1(2,:),'g','LineWidth',linewidth);
    h(4) = plotwmarker(x2,y2(2,:),linewidth,markersize,markertype,markerevery,'g');
    h(5) = plot(x1,y1(3,:),'b','LineWidth',linewidth);
    h(6) = plotwmarker(x2,y2(3,:),linewidth,markersize,markertype,markerevery,'b');   

    miny1 = min(min(y1(1,:)),min(min(y1(2,:)),min(y1(3,:))));
    miny2 = min(min(y2(1,:)),min(min(y2(2,:)),min(y2(3,:))));
    miny = min(miny1,miny2);
    maxy1 = max(max(y1(1,:)),max(max(y1(2,:)),max(y1(3,:))));
    maxy2 = max(max(y2(1,:)),max(max(y2(2,:)),max(y2(3,:))));
    maxy = max(maxy1,maxy2);
    ylim([miny maxy]);
    
    xlim([x1(1) x1(end)]);
    
    xlabel('s');  
    ylabel('m/s');    
    set_figure_params(hfig,'Estimation Velocities',fontsize,figsize);
    hleg = legend(h,'$V_{xt}$','$\hat{V_x}$','$V_{yt}$','$\hat{V_y}$','$V_{zt}$','$\hat{V_z}$');
    set(hleg,'Interpreter','latex','Fontsize',fontsize);
    drawnow
    l = legendmarkeradjust(hleg,linewidth,markersize);
    hold off
end

% Plot Sensor readings
if plotparams.sensors.enable
    [gtruth,tgt,q_s,w_s,a_s,t_imu,px4flow_s,t_px4flow,range_s,t_range] = rosbag_wrapper_load_all(path,plotparams.topics);
    plot_3vec(4,'IMU ACC.','s','m/s','m/s','m/s',t_imu,a_s,'${\bf a}_s$','b');  
    plot_3vec(5,'IMU GYRO.','s','rad/s','rad/s','rad/s',t_imu,w_s,'${\bf \omega}_s$','b');  
    for ii=1:size(q_s,2)
        e_s(:,ii) = q2e(q_s(:,ii));
    end    
    plot_3vec(6,'IMU ORI.','s','rad','rad','rad',t_imu,e_s,'${\bf e}_s$','b');  
    plot_3vec(7,'PX4 OF.','s','m','pix/s','pix/s',t_px4flow,px4flow_s(1:3,:),'${\bf px4flow}_s$','b');
    plot_3vec(8,'PX4 OF.','s','m','m/s','m/s',t_px4flow,[px4flow_s(1,:);px4flow_s(4:5,:)],'${\bf px4flow}_s$','b');
    
    figure(9)
    plot(t_range,range_s);
end

% Plot 3D Trajectories 
if plotparams.traj.enable
% Large experiment: full battery
%     cut_toff = 1;
%     cut_land = 1000;   
%     robots(2).state(1:3,:) = robots(2).state(1:3,:) + repmat(robots(1).state(1:3,1),1,length(robots(2).state));
%     robots(2).state(3,:) = robots(2).state(3,:) - repmat(robots(2).state(3,1),1,length(robots(2).state)); % remove Z offset due to take off
%     robots(1).state = robots(1).state(:,cut_toff:end-cut_land);
%     robots(1).t = robots(1).t(cut_toff:end-cut_land);
%     robots(2).state = robots(2).state(:,cut_toff:end-cut_land);
%     robots(2).t = robots(2).t(cut_toff:end-cut_land);
    
    
    hndl = plot_3Dtraj(8,plotparams,robots);
    
    fontsize = 30;
    linewidth = 2;
    markersize = 10;
    hleg = legend([hndl.robot.quad(1).odom,hndl.robot.quad(2).odom],'${\bf p}_{xt}$','$\hat{\bf p}$');
    set(hleg,'Interpreter','latex','Fontsize',fontsize);
    drawnow
    l = legendmarkeradjust(hleg,linewidth,markersize);    
end

% Plot 2D Trajectories 
if plotparams.traj2D.enable

  % Loop plot in the paper    
for ii=1:length(robots(2).t)
   [e] = q2e(robots(1).state(7:10,ii));
%    robots(1).state(1:3,ii) = robots(1).state(1:3,ii) + [0.75;-0.1;0.0];  % Loop paper plot
   robots(1).state(1:3,ii) = e2R([0.0 0.0 -e(3)])*robots(1).state(1:3,ii) + [0.75;-0.1;0.0];  % Loop paper plot
end
    
    
    fig = 9;
    y1 = robots(1).state(1:3,:);
    y2 = robots(2).state(1:3,:);

    hfig = figure(fig);
    grid on
    hold on
    
    fontsize = 30;
    linewidth = 2;
    markersize = 10;
    markertype = '*';
    markerevery = 200;
    figsize = [100 100 1280 480];
    
    % Other plots
%     h(1) = plot(y1(1,:),y1(2,:),'Color',[0.5,0.5,0.5],'LineWidth',linewidth); 
%     h(2) = plot(y2(1,:),y2(2,:),'b','LineWidth',linewidth);
    
%     % Loop plot in the paper
    h(1) = plot(y1(1,6000:end),y1(2,6000:end),'Color',[0.5,0.5,0.5],'LineWidth',linewidth); 
    h(2) = plot(y2(1,6000:end),y2(2,6000:end),'b','LineWidth',linewidth);    

%     % Heart    
%     ylim([-2.5 2.5]);    
%     xlim([-1 7]);

    % Eight
%     ylim([-2.5 2.5]);    
%     xlim([-2 2]);
    
    % Loop
%     ylim([-2 2]);    
%     xlim([-0.5 3]);    


    ylim([-2 2]);    
    xlim([-1 3]); 

    xlabel('x (m)');  
    ylabel('y (m)');    
    set_figure_params(hfig,'2D Trajectory Estimation',fontsize,figsize);
    hleg = legend([h(1) h(2)],'${\bf p}_t$','$\hat{\bf p}$');
    set(hleg,'Interpreter','latex','Fontsize',fontsize);
    drawnow
    l = legendmarkeradjust(hleg,linewidth,markersize);
    
%     axis equal
    
    hold off    
    
end

% Plot position and Orientation errors
if plotparams.trajerr.enable
    plot_trajerr(10,30,'trajectory estimation error','m','rad','s',robots);
end

% Get RMSE and STD
[error_rms,error_std,error] = calc_rms(robots(1).state(1:3,:),robots(1).t,robots(2).state(1:3,:),robots(2).t);
fprintf(['- RMSE: ',mat2str(error_rms',2),'\n']);
fprintf(['- STD: ',mat2str(error_std',2),'\n']);
