% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   PLOT SENSORS
% 
%   Main Script used to plot IMU, PX4Flow and Range sensor values
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

clear
clc
close all
format short
 
main_path = strcat(pwd,'/../');

addpath(genpath(strcat(main_path,'functions')));
addpath(genpath(strcat(main_path,'params')));
addpath(genpath(strcat(main_path,'matlab_rosbag-0.4.1-linux64')));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                                                        % Define data paths
%                                folder = 'data/real/rosbags/sensors_rigid/';
%                     bagfile_name = 'sensors_rigid_2016-03-26-23-56-38.bag';
%                     bagfile_name = 'sensors_rigid_2016-03-26-23-57-38.bag';
%                     bagfile_name = 'sensors_rigid_2016-03-26-23-59-23.bag';
                            folder = 'data/real/rosbags/sensors_isolated/';
%                  bagfile_name = 'sensors_isolated_2016-03-27-05-55-04.bag'; 
%                  bagfile_name = 'sensors_isolated_2016-03-27-05-56-08.bag'; 
                 bagfile_name = 'sensors_isolated_2016-03-27-05-56-52.bag'; 
                         plotparams.topics.gtodom = '/QuadrotorOscar/odom';
                             plotparams.topics.estodom = '/eskf_odom/odom';
                            plotparams.topics.imu = '/quad_decode_msg/imu';
                           plotparams.topics.px4flow = '/px4flow/opt_flow';
                      plotparams.topics.range = '/teraranger/timeofflight';
                      
                                        plotparams.traj.frame_length = 0.2;
                                            plotparams.traj.size_m2m = 0.2;
                      
%__________________________________________________________________________

                                                     % Plot sensor readings
                                         plotparams.sensors.enable = true;
%__________________________________________________________________________

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Generate data path
path = strcat(main_path,folder,bagfile_name);

% Plot Sensor readings
if plotparams.sensors.enable
    [gtruth,tgt,q_s,w_s,a_s,t_imu,px4flow_s,t_px4flow,range_s,t_range] = rosbag_wrapper_load_all(path,plotparams.topics);
%     plot_3vec(1,'IMU ACC.','s','m/s','m/s','m/s',t_imu,a_s,'${\bf a}_s$','b');  
%     plot_3vec(2,'IMU GYRO.','s','rad/s','rad/s','rad/s',t_imu,w_s,'${\bf \omega}_s$','b');  
    plot_3vec(3,'PX4 OF.','s','m','pix/s','pix/s',t_px4flow,px4flow_s(1:3,:),'${\bf px4flow}_s$','b');
%     plot_3vec(4,'PX4 OF.','s','m','m/s','m/s',t_px4flow,[px4flow_s(1,:);px4flow_s(4:5,:)],'${\bf px4flow}_s$','b');
end


