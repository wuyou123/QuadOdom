% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   QUADROTOR ODOMETRY
% 
%   Quadrotor state estimation using an IMU, Optical flow and Range
%   sensors. It uses EKF and ESKF as main filtering engines.
%
%   Please cite:
%   
%     A. Santamaria-Navarro, J. Solà and J. Andrade-Cetto. 
%     High-frequency MAV state estimation using low-cost inertial and 
%     optical flow measurement units, 2015 IEEE/RSJ International 
%     Conference on Intelligent Robots and Systems, 2015, Hamburg, 
%     pp. 1864-1871.
%
% 
%   SIMULATION
%
%   Simulation data can be obtained using the MATLAB Quadrotor simulation:
% 
%   QuadSim: https://github.com/angel-santamaria/QuadSim
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

clear
clc
close all
format short
 
addpath(genpath(strcat(pwd,'/functions')));
addpath(genpath(strcat(pwd,'/params')));
addpath(genpath(strcat(pwd,'/matlab_rosbag-0.4.1-linux64')));

% TODO
%todo('Add sensor extrinsic calibration parameters');

% Set filter options
[data,filter_opts] = set_filter_options();

for num_path = 1:size(data.paths,2)

    
    % Using MAT files __________________________________

    if strcmp(data.source,'TXT')

        % Load IMU data 
        [q_s,w_s,a_s,t_imu] = imu_load_data(data.paths{num_path});

        % Load PX4Flow data
        [px4flow_s,t_px4flow] = px4flow_load_data(data.paths{num_path});

        % Load Range data
        [range_s,t_range] = range_load_data(data.paths{num_path});
    
        % Load Ground Truth
        [gtruth,tgt] = gt_load_data(data.paths{num_path});  
        
        % Prepare variables to save data
        main_res_path = strrep(data.paths{num_path}, 'data', 'results');
    
    % Using ROSBAG files ________________________________

    elseif strcmp(data.source,'ROSBAG')
    
        [gtruth,tgt,q_s,w_s,a_s,t_imu,px4flow_s,t_px4flow,range_s,t_range] = rosbag_wrapper_load_all(data.paths{num_path},data.topics);
        
        % MATLAB ROS SUPPORT TOOLBOX FUNCTIONS (Not used: too slow)
        % bagfile = rosbag(data.paths{num_path});
        % [t_imu,a_s,w_s,q_s,~,~,~] = imu_from_rosbag(bagfile,data.imu_topic);
        % [t_px4flow,px4flow_s] = px4flow_from_rosbag(bagfile,data.px4flow_topic);
        % [t_range,range_s] = range_from_rosbag(bagfile,data.range_topic);    
        % [tgt,gtruth] = odom_from_rosbag(bagfile,data.gtodom_topic);

        % Prepare variables to save data
        main_res_path = strrep(data.paths{num_path}, 'data', 'results');
        main_res_path = strrep(main_res_path, '.bag', '');
    
    else
        error('Data source type not supported');
    end
   
    
    % Real Robot axis changes according to simulation
    if strfind(data.paths{num_path}, 'real')
        px4flow_s(3,:)=-px4flow_s(3,:); %OF Y axis
        px4flow_s(5,:)=-px4flow_s(5,:); %OF Y axis
        a_s(2,:)=-a_s(2,:); %Acc. Y axis
        a_s(3,:)=-a_s(3,:); %Acc. Z axis
        w_s(2,:)=-w_s(2,:); %Ang. Vel. Y axis
        w_s(3,:)=-w_s(3,:); %Ang. Vel. Z axis
    end    
    
    % Run each filter type
    for nfilter=1:size(filter_opts,2)
        
        for nrun=1:filter_opts(nfilter).n_runs
           
            % Print filter options
            print_options(filter_opts(nfilter),data.paths{num_path},nrun);

            % Initialize filter and sensor variables
            if strfind(data.paths{num_path}, 'real')
                [xtrue0,Ptrue0,Fi,Qi] = vars_ini_real(filter_opts(nfilter).met,t_imu);
                [px4flow_params] = px4flow_ini_real();
                [range_params] = range_ini_real();
            elseif strfind(data.paths{num_path}, 'simu')
                [xtrue0,Ptrue0,Fi,Qi] = vars_ini_simu(filter_opts(nfilter).met,t_imu);
                [px4flow_params] = px4flow_ini_simu();
                [range_params] = range_ini_simu();
            else
                error('The specified path name does not contain REAL or SIMU. Type "help set_filter_options" and see the NOTE for more details.');
            end
            xtrue0(1:10,1) = gtruth(1:10,1); % Initialize xstate with ground truth pose

            % Kalman filter
            tic
            [tstate,xstate,P] = Kfilter(filter_opts(nfilter),xtrue0,Ptrue0,Fi,Qi,t_imu,a_s,w_s,t_px4flow,px4flow_s,px4flow_params,t_range,range_s,range_params);
            toc           

            % Store Data      
            res_path = strcat(main_res_path,'run',num2str(nrun),'or',num2str(filter_opts(nfilter).obs_rate),'/');
            if ~exist(res_path,'dir'), mkdir(res_path); end
            fname = create_filter_name(filter_opts(nfilter));
            pname = strcat(res_path,fname);
            save(pname,'tstate','xstate','P','-v7.3')
            disp(strcat('  |_ Saving file: ./',pname));       

            % Save Ground Truth for this path
            fname = sprintf('gtruth.mat');
            pname = strcat(res_path,fname);    
            save(pname,'gtruth','tgt','-v7.3');
            disp(strcat('  |_ Saving file: ./',pname));    
    
            % Save sensor's data for this path
            fname = sprintf('sensors_data.mat');
            pname = strcat(res_path,fname); 
            save(pname,'t_imu','a_s','w_s','q_s','t_px4flow','px4flow_s','t_range','range_s','-v7.3');
            disp(strcat('  |_ Saving file: ./',pname));             
                        
            clear xstate;
            clear P;    
        end
    end
     
end

% main_plot

% emilio

