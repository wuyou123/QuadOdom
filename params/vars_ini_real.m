% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   VARS INI REAL
% 
%   Initializes all variables needed by the filter for the REAL robot.
%   This includes Filter covariance matrices, state vectors, and
%   observation covariances. 
% 
%   [xstate,P,Fi,Qi] = vars_ini_real(filter_type,t_imu)
% 
%   -Inputs:
%       - filter_type:   Filter type: 'ekf' or 'eskf'.
%       - t_imu:         Imu time vector.
%   
%   -Outputs:
%       - xstate:       Nominal State.
%       - P:            System Covariance matrix.
%       - Fi:           Jacobian of perturbations (maps IMU std dev).
%       - Qi:           Perturbation Matrix (diagonal of IMU std dev).   
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [xtrue0,Ptrue0,Fi,Qi] = vars_ini_real(filter_type,t_imu)

% Nominal and Error State Vectors %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%__________________________________________________________________POSITION
p0_est(:,1) = [0;0;0.05]; % Filter: nominal
dp0_est_std = [0;0;0.05]; % Filter: initial error

%__________________________________________________________________VELOCITY
v0_est(:,1) = [0;0;0]; % Filter: nominal
dv0_est_std = [0;0;0]; % Filter: initial error

%_______________________________________________________________ORIENTATION
q0_est(:,1) = e2q([0;0;0]); % Filter: nominal
dtheta0_est_std = 5e-2*[1;1;1]; % Filter: initial error

%___________________________________________________________________GRAVITY
g0_est = [0;0;-9.803057]; % Filter: nominal
dg0_est_std = [0;0;0]; % Filter: initial error

%______________________________________________________________________ACC.
a_est_std_cont = 0.2; % Sensor: Acc. noise. (High value due to vibrations)

% ______________________________________________________________ACC. BIASES
ab0_est = [0;0;0]; % Filter: nominal
dab0_est_std = 2e-3*9.8*[1;1;1]; % Filter: initial error
ab_est_std = 0; % Sensor: Acc. biases noise (random walk).
                                     
%_____________________________________________________________________GYRO.
w_est_std_cont = 0.005; % Sensor: Gyros. noise.

% _____________________________________________________________GYRO. BIASES
wb0_est = [-0.0005;-0.0026;-0.0004]; % Filter: nominal
dwb0_est_std = [0.0037;0.0040;0]; % Filter: error (yaw non observable)
wb_est_std = 0; % Sensor: Gyro. biases noise (random walk).
                                    

% Jacobians and Covariance matrices %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% IMU Perturbation Jacobian and Covariance matrix
dt_imu = t_imu(2)-t_imu(1);
std_est_imu_cont = [a_est_std_cont;w_est_std_cont;ab_est_std;wb_est_std]; %a,w,ab,wb
var_est_imu_cont = std_est_imu_cont.^2;
var_est_imu_dis = var_est_imu_cont.*[dt_imu^2;dt_imu^2;dt_imu;dt_imu];
var_est_imu_dis = kron(var_est_imu_dis,ones(3,1));

Qi=diag(var_est_imu_dis);
Fi=zeros(18,12);
Fi(4:15,:)=eye(12);

% Filter covariance matrix
std_ini=[dp0_est_std;dv0_est_std;[dtheta0_est_std(1:2);0];dab0_est_std;dwb0_est_std;dg0_est_std]; % Yaw at 0, not moving

switch lower(filter_type)
    case {'eskf'}
        Ptrue0 = zeros(18,18);
        Ptrue0(:,:) = diag(std_ini.^2);
    case {'ekf'}
        % Covariance quaternion part of P needs to be rotated according to
        % the theta to quat mapping
        Qe = 0.5*q2Pi(q0_est(:,1));
        Ptrue0 = zeros(19,19);
        Ptrue0(1:6,1:6) = diag(std_ini(1:6).^2);
        Ptrue0(7:10,7:10) = Qe*diag(std_ini(7:9).^2)*Qe';
        Ptrue0(11:19,11:19) = diag(std_ini(10:18).^2);
end        

% Nominal State init____________________________________
xtrue0(:,1) = [p0_est(:,1);v0_est(:,1);q0_est(:,1);ab0_est(:,1);wb0_est(:,1);g0_est];

return