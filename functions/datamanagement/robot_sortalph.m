% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   SYNCHRONIZE ROBOTS
% 
%   This functions returns the synchronized estimation w.r.t. the ground truth
%   considering their time stamps. The output robot has the size as the
%   ground truth to facilitate plots.
% 
%   - Inputs:
%       - Rob:      Robot to be synchronized.
%       - GTRob:    Ground Truth Robot. 
%   - Outputs:
%       - Rob_sync: Synchronized Robot.
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [Rob_sorted] = robot_sortalph(Rob)

Rob_sorted = Rob;

for ii = 2:size(Rob,2)
    names{ii-1} = Rob(ii).name;
end

[~,idx] = sort(names);

for ii = 2:size(Rob,2)
   Rob_sorted(ii) = Rob(idx(ii-1)+1); 
end

return