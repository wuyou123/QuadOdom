% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   WHICH IS NEXT?
% 
%   Shows which sensor from SENSOR cells is the next to be processed
%   according to the times in TIMES.
%
%   function [next] = which_is_next(sensors,times)
% 
%    - Inputs:
%       - sensors:  Sensor types (cell of strings)
%       - times:    Sensor times (vector of doubles)
% 
%   - Outputs:
%       - next:     String of next sensor to be processed.
%   
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [next] = which_next(sensors,times)

[~,M] = min(times);
next = sensors{M};

return