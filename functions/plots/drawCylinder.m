% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadSim. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadSim is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadSim.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   Draw Cylinder
% 
%   [hCyl] = drawCylinder(parent,X1,X2,r,n,cyl_color,closed,lines,alpha)
% 
%   This function constructs a cylinder connecting two center points 
% 
%   - Inputs:
%       - parent:       Parent handler where to draw the cylinder.
%       - X1:           3D vector containing point 1. 
%       - X2:           3D vector containing point 2.
%       - r:            Radius of the cylinder.
%       - n:            No. of elements on the cylinder circumference (more--> refined).    
%       - cyl_color:    Color definition.
%       - closed:       Colsed cylinder (1) or hollow open cylinder (0). 
%       - lines:        Display the lines segments (1), or not (0).
%       - alpha:        Surface alpha.
% 
%   - Outputs:
%       - hCyl:     Cylinder Handler.    
% 
%   NOTE: There is a MATLAB function "cylinder" to revolve a curve about an
%   axis. This "Cylinder" provides more customization like direction and etc
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [hCyl] = drawCylinder(parent,X1,X2,r,n,cyl_color,closed,lines,alpha)

% Calculating the length of the cylinder
length_cyl=norm(X2-X1);

% Creating a circle in the YZ plane
t=linspace(0,2*pi,n)';
x2=r*cos(t);
x3=r*sin(t);

% Creating the points in the X-Direction
x1=[0 length_cyl];

% Creating (Extruding) the cylinder points in the X-Directions
xx1=repmat(x1,length(x2),1);
xx2=repmat(x2,1,2);
xx3=repmat(x3,1,2);

% Drawing two filled cirlces to close the cylinder
if closed==1
    hold on
    EndPlate1=fill3(xx1(:,1),xx2(:,1),xx3(:,1),'r');
    EndPlate2=fill3(xx1(:,2),xx2(:,2),xx3(:,2),'r');
end

% Plotting the cylinder along the X-Direction with required length starting
% from Origin
Cylinder=mesh(xx1,xx2,xx3,'Parent',parent);

% Defining Unit vector along the X-direction
unit_Vx=[1 0 0];

% Calulating the angle between the x direction and the required direction
% of cylinder through dot product
angle_X1X2=acos( dot( unit_Vx,(X2-X1) )/( norm(unit_Vx)*norm(X2-X1)) )*180/pi;

% Finding the axis of rotation (single rotation) to roate the cylinder in
% X-direction to the required arbitrary direction through cross product
axis_rot=cross([1 0 0],(X2-X1) );

% Rotating the plotted cylinder and the end plate circles to the required
% angles
if angle_X1X2~=0 % Rotation is not needed if required direction is along X
    rotate(Cylinder,axis_rot,angle_X1X2,[0 0 0])
    if closed==1
        rotate(EndPlate1,axis_rot,angle_X1X2,[0 0 0])
        rotate(EndPlate2,axis_rot,angle_X1X2,[0 0 0])
    end
end

% Till now cylinder has only been aligned with the required direction, but
% position starts from the origin. so it will now be shifted to the right
% position
if closed==1
    set(EndPlate1,'XData',get(EndPlate1,'XData')+X1(1))
    set(EndPlate1,'YData',get(EndPlate1,'YData')+X1(2))
    set(EndPlate1,'ZData',get(EndPlate1,'ZData')+X1(3))
    
    set(EndPlate2,'XData',get(EndPlate2,'XData')+X1(1))
    set(EndPlate2,'YData',get(EndPlate2,'YData')+X1(2))
    set(EndPlate2,'ZData',get(EndPlate2,'ZData')+X1(3))
end
set(Cylinder,'XData',get(Cylinder,'XData')+X1(1))
set(Cylinder,'YData',get(Cylinder,'YData')+X1(2))
set(Cylinder,'ZData',get(Cylinder,'ZData')+X1(3))

% Setting the color to the cylinder and the end plates
set(Cylinder,'FaceColor',cyl_color)
if closed==1
    set([EndPlate1 EndPlate2],'FaceColor',cyl_color)
else
    EndPlate1=[];
    EndPlate2=[];
end

% If lines are not needed making it disapear
if lines==0
    set(Cylinder,'EdgeAlpha',0)
end

set(Cylinder,'FaceAlpha',alpha);
set(EndPlate1,'FaceAlpha',alpha);
set(EndPlate2,'FaceAlpha',alpha);

hCyl = [Cylinder EndPlate1 EndPlate2];

return