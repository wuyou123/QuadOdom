% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   Plot 3D Trajectories
% 
%   [] = legendmarkeradjust(marksize)
% 
%   Adjusts the marker size inside the legend of the current figure.
% 
%   - Inputs:
%       - markersize:   New size for the markers.
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________


function hndl = legendmarkeradjust(hndl,LineWidth,MarkerSize)

entries = hndl.EntryContainer.Children;
for i = 1:numel(entries)
    primitives = entries(i).Icon.Transform.Children.Children;
    for j=1:numel(primitives)
        if(isa(primitives(j),'matlab.graphics.primitive.world.Marker'))
            primitives(j).Size = MarkerSize;
            primitives(j).LineWidth = LineWidth;    
        end
        if(isa(primitives(j),'matlab.graphics.primitive.world.LineStrip'))
            primitives(j).LineWidth = LineWidth;            
        end        
    end
end

return
