% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   Plot Trajectory Error
% 
%   [hndl] =  plot_trajerr(fig,fontsize,name,y1lab,y2lab,xlab,robots)
% 
%   Plots the Linear and Angular errors between two trajectories.
% 
%   - Inputs:
%       - fig:          Figure number.
%       - fontsize:     Font size.
%       - name:         Figure name.
%       - y1lab:        Linear Error vertical axis label.
%       - y2lab:        Angular Error vertical axis label.
%       - xlab:         Horizontal axis label (Time).
%       - robots:       State and time trajectories in robots structure 
%                       (see newrobot.m)
% 
%   - Outputs:
%       - hndl:     Figure Handle.
%   
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [hndl] =  plot_trajerr(fig,fontsize,name,y1lab,y2lab,xlab,robots)

hfig = figure(fig);

e_pos = zeros(3,size(robots(1).state,2));
e_ang = zeros(3,size(robots(1).state,2));

for ii=1:size(robots(1).state,2)
   e_pos(:,ii) = robots(1).state(1:3,ii)-robots(2).state(1:3,ii);
   e_ang(:,ii) = q2e(qProd(robots(1).state(7:10,ii),robots(2).state(7:10,ii)));
end

t = robots(1).t;

h11=subplot(2,1,1);hold on; plot(t,e_pos(1,:),'r');plot(t,e_pos(2,:),'g');plot(t,e_pos(3,:),'b');
ylabel(y1lab); 


miny = min(min(e_pos(1,:)),min(min(e_pos(2,:)),min(e_pos(3,:))));
maxy = max(max(e_pos(1,:)),max(max(e_pos(2,:)),max(e_pos(3,:))));
ylim([miny maxy])

h12=subplot(2,1,2);hold on; plot(t,e_ang(1,:),'r');plot(t,e_ang(2,:),'g');plot(t,e_ang(3,:),'b');
ylabel(y2lab); 
xlabel(xlab); 
miny = min(min(e_ang(1,:)),min(min(e_ang(2,:)),min(e_ang(3,:))));
maxy = max(max(e_ang(1,:)),max(max(e_ang(2,:)),max(e_ang(3,:))));
ylim([miny maxy])

set_figure_params(hfig,name,fontsize,[100 100 1050 640]);

linkaxes([h11 h12],'x');
xlim([min(t) max(t)]);

hndl = [hfig h11 h12];

hold off

return
