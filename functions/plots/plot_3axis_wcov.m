% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   Plot 3-axis vector with covariances  
% 
%   [] = plot_3axis_wcov(fig,name,x,y1,cov,lab1,c1,lab2,c2,lab3,c3,xlab,ylab,xlines,leg,axs)
% 
%   Plots a 3-vector with associated covariances in the same plot
% 
%   - Inputs:
%       - fig:      Figure number.
%       - name:     Figure name.
%       - x:        X axis data.
%       - y1:       3-Vec data.
%       - cov:      3-Vec covariances.
%       - lab1:     Var 1 label.
%       - c1:       Var 1 color.
%       - lab2:     Var 2 label.
%       - c2:       Var 2 color.
%       - lab3:     Var 3 label.
%       - c3:       Var 3 color.
%       - xlab:     X axis label.
%       - ylab:     Y axis label.
%       - xlines:   Vertical lines X coordinates (optional).
%       - leg:      Legend flag (boolean).
%       - axs:      Figure axis.
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [] = plot_3axis_wcov(fig,name,cov_zcenter,x,y1,cov,lab1,c1,lab2,c2,lab3,c3,xlab,ylab,xlines,leg,axs)

hfig = figure(fig);   
fontsize = 30;
set_figure_params(hfig,name,fontsize,[0 0 1300 300])

% Common variables
clr=[c1;c2;c3]; % line colors
lw = 3; % line width
ls='-'; % line style
sqcov=sqrt(cov); %covariances

for ii=1:3:9
    row = 1+floor(ii/3);
    h(ii)=plot(x,y1(row,:),'Color',clr(row,:),'LineStyle',ls,'LineWidth',lw); 

    if cov_zcenter 
        h(ii+1)=plot(x,3*sqcov(row,:),'Color',clr(row,:),'LineStyle','-.','LineWidth',lw);
        h(ii+2)=plot(x,-3*sqcov(row,:),'Color',clr(row,:),'LineStyle','-.','LineWidth',lw);        
    else
        h(ii+1)=plot(x,y1(row,:)+3*sqcov(row,:),'Color',clr(row,:),'LineStyle','-.','LineWidth',lw);
        h(ii+2)=plot(x,y1(row,:)-3*sqcov(row,:),'Color',clr(row,:),'LineStyle','-.','LineWidth',lw);
    end
end


% Plot verical lines (Optional)
if nargin>14
    yax=ylim;
    for ii=1:size(xlines,2)
    x=[xlines(1,ii),xlines(1,ii)];
    y=[yax(1,1),yax(1,2)];
    plot(x,y,'--k'); 
    end
    
    if (leg)
        h=legend([h(1) h(4) h(7) h(10)],'$G.Truth$',lab1,lab2,lab3);
        legendmarkeradjust(50);
        set(h,'interpreter','latex','fontsize',200,'Location','NorthEast','orientation','horizontal','Units','pixels');
    end

end
if nargin > 16
    axis(axs);
end

xlim([min(x) max(x)]);


xlabel(xlab);
ylabel(ylab);

hold off

return