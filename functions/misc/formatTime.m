% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
% 
%   Format Time
%
%   str = formatTime(float_time)
%
%   Inputs:
%       - float_time:   Time in seconds.
% 
%   Outputs:
%       - str: Formatted time: HH:MM:SS
%
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function str = formatTime(float_time)
    % Format the Time String
    float_time = abs(float_time);
    hrs = floor(float_time/3600);
    mins = floor(float_time/60 - 60*hrs);
    secs = float_time - 60*(mins + 60*hrs);
    h = sprintf('%1.0f:',hrs);
    m = sprintf('%1.0f:',mins);
    s = sprintf('%1.3f',secs);
    if hrs < 10
        h = sprintf('0%1.0f:',hrs);
    end
    if mins < 10
        m = sprintf('0%1.0f:',mins);
    end
    if secs < 9.9995
        s = sprintf('0%1.3f',secs);
    end
    str = [h m s];
end