% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
% 
%   Mean and covariance reset considering that quaternion needs 
%   to be normalized and its covariance updated accordingly.
%
%   [xstate,P]=EKFreset(xstate_old,P_old)
%
%   Returns current state vector and its covariance.
%
%   Inputs:
%       - xstate_old:   Last state vector.
%       - P_old:        State covariance matrix.
%   Ouputs:
%       - xstate: 	New state vector.
%       - P:        New state covariance matrix.
%
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [xstate,P]=EKFreset(xstate_old,P_old)

% Quaternion normalization
xstate = xstate_old;
[q, Qq] = qNorm(xstate(7:10)');
xstate(7:10) = q';

% Covariance update 
P = P_old;
P(7:10,:) = Qq*P(7:10,:);
P(:,7:10) = P(:,7:10)*Qq';

return