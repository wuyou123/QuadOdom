% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
% 
%   Kalman Filter Correction
%
%   [state,P] = correction(state_old,dz,Z,H,P_old)
%
%   Returns the correction of the error state vector and its covariance 
%   matrix considering the observation Jacobian and the residual.
%   
%   Inputs:
%       - state_old:    Previous State vector.
%       - dz:          	Residual of the innovation.
%       - Z:            Observation covariance matrix.
%       - H:            Observation model Jacobian.
%       - P_old:        Covariance matrix at time k-1.
% 
%   Outputs:
%       - dxstate:      Error state vector at time k.
%       - P:            Covariance matrix at time k.
%
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [state,P] = correction(state_old,dz,Z,H,P_old)

%Kalman Gain
% K = P_old * H' * inv(Z); %--> less accurate b*inv(a) than b/a.
K = P_old * H'/Z;

%Correction
state = state_old + K * dz;

%New covariance forcing a positive and symetric matrix
% K2 = (eye(18) - K * H);
% P = K2*P_old*K2' + K*Vof*K'; 
P = P_old - K*Z*K';
P = (P + P')/2;

return