% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   GET ODOMETRY
% 
%   [found,tstate,xstate] = rosbag_wrapper_getodom(bag,topic)
% 
%   This functions reads an Odometry message from a ROSBAG file and returns, 
%   if found, the time vector and robot state.
% 
%   - Inputs:
%       - bag:      Bag file loaded with rosbag_wrapper library.
%       - topic:    Odometry topic name.
% 
%   - Outputs:
%       - found:    Flag if found (1: found, 0: not found)
%       - tstate:   Time vector.
%       - xstate:   Odometry state [p v q w].
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [found,tstate,xstate] = rosbag_wrapper_getodom(bag,topic)

found = false;
tstate = 0;
xstate = [];

% Ground Truth Odometry
tops = strfind(bag.topics,topic);
if ~isempty([tops{~cellfun(@isempty,strfind(bag.topics,topic))}]) 
    [msgs, meta] = bag.readAll(topic);
    accessor = @(odometry) odometry.pose.pose.position;
    [p] = ros.msgs2mat(msgs, accessor); 
    accessor = @(odometry) odometry.pose.pose.orientation;
    [q] = ros.msgs2mat(msgs, accessor); 
    q = rosq2matq(q);
    accessor = @(odometry) odometry.twist.twist.linear;
    [vlin] = ros.msgs2mat(msgs, accessor);
    accessor = @(odometry) odometry.twist.twist.angular;
    [vang] = ros.msgs2mat(msgs, accessor);
    xstate = [p;vlin;q;vang];
    tstate = cellfun(@(x) x.time.time, meta);
    tstate = tstate-repmat(tstate(1),1,size(tstate,2));
    found = true;
else
    warning(strcat('Topic: ',topic,' not found'));
end

return



