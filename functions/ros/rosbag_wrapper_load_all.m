% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   LOAD ALL SENSORS AND GT DATA FROM A ROSBAG FILE (USING ROS_WRAPPER)
% 
%   [gtruth,tgt,q_s,w_s,a_s,t_imu,px4flow_s,t_px4flow,range_s,t_range] = rosbag_wrapper_load_all(bagfile,topics)
% `
%   This functions reads the Ground truth and and all required sensor 
%   readings from a ROSBAG file.
% 
%   - Inputs:
%       - bagfile:  Bag file path and with file name.
%       - topics:   Grodun truth and sensor topic names.
% 
%   - Outputs:
%       - gtruth:       Ground truth state. [p;v;q;w]
%       - tgt:          Ground truth time vector.
%       - q_s:          IMU orientation readings. Hamilton quaternions [qw;qx;qy;qz]
%       - w_s:          IMU angular velocity readings.
%       - a_s:          IMU acc. readings.
%       - t_imu:        IMU time vector.
%       - px4flow_s:    PX4Flow readings. [range;flow_x;flow_y;velocity_x;velocity_y]
%       - t_px4flow:    PX4Flow time vector.
%       - range_s:      Range readings.
%       - t_range:      Range time vector.
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [gtruth,tgt,q_s,w_s,a_s,t_imu,px4flow_s,t_px4flow,range_s,t_range] = rosbag_wrapper_load_all(bagfile,topics)

% Load bagfile
bag = ros.Bag.load(bagfile);

% Ground Truth Odometry
disp('-> Loading GROUND TRUTH data...');
[msgs, meta] = bag.readAll(topics.gtodom);
accessor = @(odometry) odometry.pose.pose.position;
[p] = ros.msgs2mat(msgs, accessor); 
accessor = @(odometry) odometry.pose.pose.orientation;
[q] = ros.msgs2mat(msgs, accessor); 
q = rosq2matq(q);
accessor = @(odometry) odometry.twist.twist.linear;
[vlin] = ros.msgs2mat(msgs, accessor);
accessor = @(odometry) odometry.twist.twist.angular;
[vang] = ros.msgs2mat(msgs, accessor);
gtruth = [p;q;vlin;vang];
tgt = cellfun(@(x) x.time.time, meta);
tgt = tgt-repmat(tgt(1),1,size(tgt,2));

% IMU
disp('-> Loading IMU data...');
[msgs, meta] = bag.readAll(topics.imu);
accessor = @(imu) imu.linear_acceleration;
[a_s] = ros.msgs2mat(msgs, accessor); 
accessor = @(imu) imu.angular_velocity;
[w_s] = ros.msgs2mat(msgs, accessor); 
accessor = @(imu) imu.orientation;
[q_s] = ros.msgs2mat(msgs, accessor); 
[q_s] = rosq2matq(q_s);
t_imu = cellfun(@(x) x.time.time, meta);
t_imu = t_imu-repmat(t_imu(1),1,size(t_imu,2));

% PX4Flow
disp('-> Loading PX4Flow data...');
[msgs, meta] = bag.readAll(topics.px4flow);
accessor = @(opticalflow) opticalflow.ground_distance;
[gdist] = ros.msgs2mat(msgs, accessor); 
accessor = @(opticalflow) opticalflow.flow_x;
[flow_x] = double(ros.msgs2mat(msgs, accessor)); 
accessor = @(opticalflow) opticalflow.flow_y;
[flow_y] = double(ros.msgs2mat(msgs, accessor)); 
accessor = @(opticalflow) opticalflow.velocity_x;
[velocity_x] = ros.msgs2mat(msgs, accessor); 
accessor = @(opticalflow) opticalflow.velocity_y;
[velocity_y] = ros.msgs2mat(msgs, accessor); 
px4flow_s = [gdist;flow_x;flow_y;velocity_x;velocity_y];
t_px4flow = cellfun(@(x) x.time.time, meta);
t_px4flow = t_px4flow-repmat(t_px4flow(1),1,size(t_px4flow,2));

% Range
disp('-> Loading RANGE data...');
[msgs, meta] = bag.readAll(topics.range);
accessor = @(range) range.range;
[range] = ros.msgs2mat(msgs, accessor); 
range_s = range;
t_range = cellfun(@(x) x.time.time, meta);
t_range = t_range-repmat(t_range(1),1,size(t_range,2));

return