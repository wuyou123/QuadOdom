% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   GET ROBOTS
% 
%   [robots] = rosbag_wrapper_getrobots(bagfile,topics,params)
% 
%   This functions reads the Ground truth and Estimation Odometry messages 
%   from a ROSBAG file and returns them un robots structure (see newrobot.m).
% 
%   - Inputs:
%       - bagfile:  Bag file path and with file name.
%       - params:   General plot parameters.
% 
%   - Outputs:
%       - robots:   Robots structure with estimation and ground truth data.
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [robots] = rosbag_wrapper_getrobots(bagfile,params)

% Load bagfile
bag = ros.Bag.load(bagfile);

% Ground Truth Odometry
disp('-> Loading GROUND TRUTH data...');
[found(1),tgt,gtruth] = rosbag_wrapper_getodom(bag,params.topics.gtodom);

% Estimation Odometry
disp('-> Loading ODOMETRY ESTIMATION data...');
[found(2),tstate,xstate] = rosbag_wrapper_getodom(bag,params.topics.estodom);

color = lines(sum(found));

if found(1)
    robots(1) = newrobot('gtruth',color(1,:),tgt,gtruth,[],params.rob.size_m2m,params.rob.frame_length);
else
    robots(1) = newrobot('gtruth',color(1,:),[],[],[],params.rob.size_m2m,params.rob.frame_length);
end

if found(2)
   robots(2) = newrobot('est',color(2,:),tstate,xstate,zeros(19,19,size(xstate,2)),params.rob.size_m2m,params.rob.frame_length);
end
    
return