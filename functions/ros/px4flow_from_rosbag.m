% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   PX4Flow from ROSBAG
% 
%   [tstamp,px4flow_s] = px4flow_from_rosbag(bagfile,topic_name)
% 
%   This script retrieves PX4Flow messages from a robsbag containing an 
%   PX4Flow topic. Messages must be already set up in matlab environment.
%   See http://www.mathworks.com/help/robotics/ug/ros-custom-message-support.html
%   and http://www.mathworks.com/help/robotics/ref/rosgenmsg.html
% 
%   - Inputs:
%       - bagfile:      Rosbag file (see 'help rosbag').
%       - topic_name:   Topic name.
% 
%   - Outputs:
%       - tstamp:       Messages time stamp. Initialized w.r.t. bag time.
%       - px4flow_s:    PX4Flow readings. [range flow2d Vxy]
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [tstamp,px4flow_s] = px4flow_from_rosbag(bagfile,topic_name)

disp('-> Loading PX4FLOW data...')

% Get topic
Data = select(bagfile,'Topic',topic_name);

% Get messages
Msgs = readMessages(Data);

% Initialize structures
numMsgs = size(Msgs,1);
tstamp = zeros(1,numMsgs);
px4flow_s = zeros(5,numMsgs);

% get initial BAG time
tini = double(bagfile.StartTime);

% Mount output structures
for ii = 1:numMsgs

   tstamp(1,ii) = double(Msgs{ii}.Header.Stamp.Sec) ...
                  + double(Msgs{ii}.Header.Stamp.Nsec)*1e-9 ...
                  - tini;
                           
   px4flow_s(:,ii) = [double(Msgs{ii}.GroundDistance); ...
                      double(Msgs{ii}.FlowX); ...
                      double(Msgs{ii}.FlowY); ...
                      double(Msgs{ii}.VelocityX); ...
                      double(Msgs{ii}.VelocityY)];
  
end

return