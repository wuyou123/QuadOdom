% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   PLOT
% 
%   Main Script used to plot estimation results
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________
% 
clear
clc
close all
format short
 
addpath(genpath(strcat(pwd,'/functions')));
addpath(genpath(strcat(pwd,'/params')));
addpath(genpath(strcat(pwd,'/matlab_rosbag-0.4.1-linux64')));

disp('');
disp('Plotting...');
disp('');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                                             %plotparams.source = 'ROSBAG';
                                                 plotparams.source = 'MAT';
                                                       
                                                        % Define data paths  
                                                folders{1} = '/data/simu/';

                                                           % Number of runs
                                                                 nruns = 1;
                                        
                                                        % Observation rates
                                             plotparams.rob.obs_rate = [1];
%                                 plotparams.rob.obs_rate = [1 2 4 10 100]; 
                                                                 
                                                        % Single path plots
                                                              num_path = 1;
                                                              
                                                                     % Misc
                                         plotparams.rob.frame_length = 0.2;
                                             plotparams.rob.size_m2m = 0.2;                                                            
%__________________________________________________________________________                                                              
if strcmp(plotparams.source,'ROSBAG')
                                                               % If needed,
                                                 % define ROSBAG file names 
                                       bagfile_names{1} = 'perch_test.bag';
                         plotparams.topics.gtodom = '/QuadrotorOscar/odom';
                             plotparams.topics.estodom = '/eskf_odom/odom';
end       
%__________________________________________________________________________

                                                     % Plot sensor readings
                                         plotparams.sensors.enable = false;
%__________________________________________________________________________

                                                     % Plot 3D trajectories
                                            plotparams.traj.enable = true;
                                           plotparams.traj.arena = 'empty';
                                           plotparams.traj.draw_every = 10;
                        plotparams.traj.scene_size = [-2 3.1 -2.5 2.5 0 5];
                                             plotparams.traj.view.az = -50;
                                              plotparams.traj.view.el = 20;
                              plotparams.traj.scene_center = [0.0;0.0;0.0];

%__________________________________________________________________________

                 % Plot estimation errors and covariances for an estimation
                                         plotparams.estwcov.enable = false;
                          plotparams.estwcov.robot_name = 'EKF_{L+N1+Q0F}';
                                     plotparams.estwcov.plot_errors = true;

%__________________________________________________________________________
                                        
                                               % Plot Trajectory stadistics
                                  plotparams.traj_stadistics.enable = true;
                              plotparams.traj_stadistics.plot5comp = false;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Create all folder with runs
main_folders = folders;

clear folders;
for ff = 1:size(main_folders,2)
    for ii = 1:nruns
        for qq = 1:size(plotparams.rob.obs_rate,2)
            folders{(((ff-1)*nruns+ii)-1)*size(plotparams.rob.obs_rate,2)+qq} = strcat(main_folders{ff},'run',num2str(ii),'or',num2str(plotparams.rob.obs_rate(qq)),'/');
            orates((((ff-1)*nruns+ii)-1)*size(plotparams.rob.obs_rate,2)+qq) = plotparams.rob.obs_rate(qq);
        end
    end
end

if strcmp(plotparams.source,'ROSBAG')    
    for ii=1:size(folders,2)
        data.paths{ii} = strcat(pwd,folders{ii},bagfile_names{ii});
    end   
elseif strcmp(plotparams.source,'MAT')  
    for ii=1:size(folders,2)
        data.paths{ii} = strcat(pwd,folders{ii});
    end   
else
    error('Wrong data source type.');
end

% Get all robots from specified path
robots = [];
for ii=1:size(data.paths,2)   
    if strcmp(plotparams.source,'ROSBAG')
        robotstmp = rosbag_wrapper_getrobots(data.paths{ii},plotparams);
    else
        robotstmp = getrobots_matfiles(data.paths{ii},plotparams.rob,orates(ii));         
    end 
    
    % Set ground truth as the first
    [robotstmp] = set_gtrob_first(robotstmp);    
    
    % Synchronize robots (same time of samples)
    for jj=2:size(robotstmp,2)  
        robotstmp(jj) = robotsync(robotstmp(jj),robotstmp(1));
    end
    
    % Compute ANEES and NEES w.r.t. corresponding ground truth
    if plotparams.traj_stadistics.enable
        disp(strcat('...Computing robotNEESPSE for path: ',data.paths{ii}));
        error(ii) = robotNeesPse(robotstmp);
    end
    
   robots = [robots robotstmp];
end

% Set ground truth as first robot
[robots] = set_gtrob_first(robots);

% get minimum length and count GT
ml = length(robots(1).t);
mlidx = 1;
ngt = 0;
for ii=1:size(robots,2)  
    if ml > min(ml,length(robots(ii).t))
        ml = min(ml,length(robots(ii).t));
        mlidx = ii;
    end
    if strcmp(robots(ii).name,'gtruth')
        ngt = ngt + 1;
    end
end

% Synchronize robots with the smallest num samples
for ii=1:size(robots,2)  
    robots(ii) = robotsync(robots(ii),robots(mlidx));
end

% Order alphabetically
robots(ngt:end) = robot_sortalph(robots(ngt:end));


% Plot Sensor readings
if plotparams.sensors.enable
    res_path = strrep(data.paths{num_path}, 'data', 'results');
    load([res_path 'sensors_data.mat']);
    plot_3vec(1,'IMU ACC.','s','m/s','m/s','m/s',t_imu,a_s,'${\bf a}_s$','b');  
    plot_3vec(2,'IMU GYRO.','s','rad/s','rad/s','rad/s',t_imu,w_s,'${\bf \omega}_s$','b');  
    plot_3vec(3,'PX4 OF.','s','m','pix/s','pix/s',t_px4flow,px4flow_s(1:3,:),'${\bf px4flow}_s$','b');
    plot_3vec(4,'PX4 OF.','s','m','m/s','m/s',t_px4flow,[px4flow_s(1,:);px4flow_s(4:5,:)],'${\bf px4flow}_s$','b');
end

% Plot 3D Trajectories 
if plotparams.traj.enable
    plot_3Dtraj(5,plotparams,robots);
end

% Plot estimation with covariances
if plotparams.estwcov.enable
    plot_estwcov(6,plotparams,robots);
end

% Compute RMS and STD for all robots
if plotparams.traj_stadistics.enable
%     error = traj_stadistics(data.paths,plotparams);
end




